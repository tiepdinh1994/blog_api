import { Component, OnInit, ViewChild } from '@angular/core';
import { DeleteBlogService } from '../services/delete-blog.service';
import { blogpost } from '../models/blogpost';
import { Router } from '@angular/router';

@Component({
  selector: 'app-create-blog',
  templateUrl: './create-blog.component.html',
  styleUrls: ['./create-blog.component.css']
})
export class CreateBlogComponent implements OnInit {

  constructor(private blogService: DeleteBlogService, private router: Router) { }

  @ViewChild('img') img
  selectedImg: any

  ngOnInit(): void {
  }

  changeImg(_img){
    // console.log(_img)
    if(_img.target.files.length > 0 && _img.target.files[0]){
      this.selectedImg = _img.target.files[0]
    }
  }

  createBlog(title, content){
    var post = new blogpost()
    post.blogTitle = title
    post.blogContent = content

    console.log(this.selectedImg)

    this.blogService.createBlog(post, this.selectedImg).subscribe(resp=>{
      console.log(resp)
      this.router.navigateByUrl('/')
    })
  }

}
