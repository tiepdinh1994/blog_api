import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BlogsComponent } from './blogs/blogs.component';
import { BlogDetailComponent } from './blog-detail/blog-detail.component';
import { LoginComponent } from './login/login.component';
import { GallaryComponent } from './layout/gallary/gallary.component';
import { CreateBlogComponent } from './create-blog/create-blog.component';


const routes: Routes = [
  { path: '', component: BlogsComponent, pathMatch: 'full' },
  { path: 'detailsBlog', component: BlogDetailComponent },
  { path: 'login', component: LoginComponent },
  { path: 'gallary', component: GallaryComponent },
  { path: 'createBlog', component: CreateBlogComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
