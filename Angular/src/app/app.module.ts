import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BlogsComponent } from './blogs/blogs.component';
import { BlogDetailComponent } from './blog-detail/blog-detail.component';
import { HeaderComponent } from './layout/header/header.component';
import { FooterComponent } from './layout/footer/footer.component';
import { BlogPostService } from './services/blog-post.service';
import { HttpClientModule } from '@angular/common/http';
import { GallaryComponent } from './layout/gallary/gallary.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { EditComponent } from './edit/edit.component';
import { DeleteBlogService } from './services/delete-blog.service';
import { CreateBlogComponent } from './create-blog/create-blog.component';
import { CommentComponent } from './Comment-Folder/comment/comment.component';
@NgModule({
  declarations: [
    AppComponent,
    BlogsComponent,
    BlogDetailComponent,
    HeaderComponent,
    FooterComponent,
    GallaryComponent,
    LoginComponent,
    RegisterComponent,
    EditComponent,
    CreateBlogComponent,
    CommentComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [BlogPostService, BlogDetailComponent, DeleteBlogService],
  bootstrap: [AppComponent]
})
export class AppModule { }
