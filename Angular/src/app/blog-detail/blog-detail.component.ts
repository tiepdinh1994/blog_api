import { Component, OnInit, Input } from '@angular/core';
import { BlogPostService } from '../services/blog-post.service';
import { blogpost } from '../models/blogpost';
import { Observable } from 'rxjs';
import { BlogDetailService } from '../services/blog-detail.service';
import {Router, ActivatedRoute} from '@angular/router'
import { DeleteBlogService } from '../services/delete-blog.service';
@Component({
  selector: 'app-blog-detail',
  templateUrl: './blog-detail.component.html',
  styleUrls: ['./blog-detail.component.css']
})
export class BlogDetailComponent implements OnInit {
  @Input() message;

  blogPosts: any;
  blogID: number;
  comPosts: any;

  constructor(private route: ActivatedRoute
    , public blogDetailService: BlogDetailService
    , private deleteBlogService: DeleteBlogService
    , private router: Router ) { }

  ngOnInit(): void {


    // thieu cai nay
    var queryParams:any = this.route.queryParams
    this.blogID = queryParams._value.id

    this.loadBlogsComment();
    this.loadBlogs();
  }
  loadBlogs() {
    this.blogDetailService.getdetails(this.blogID).subscribe(resp=>{
      console.log(resp)
      this.blogPosts = resp
    }, error=>{
      alert(error.message)
      console.log(error)
    })
  }
  loadBlogsComment() {
    this.blogDetailService.getdetails(this.blogID).subscribe(resp=>{
      console.log(resp)
      this.comPosts = resp
    }, error=>{
      alert(error.message)
      console.log(error)
    })
  }





  deletePost(){
    this.deleteBlogService.deleteBlog(this.blogID).subscribe(resp=>{
      console.log(resp)
        this.router.navigateByUrl('/')
      }, error=>{
      alert(error.message)
    })
  }

  ///////////////////////////
  createPost(){
    this.deleteBlogService.deleteBlog(this.blogID).subscribe(resp=>{
      console.log(resp)
        this.router.navigateByUrl('/')
      }, error=>{
      alert(error.message)
    })
  }
  
}
