import { TestBed } from '@angular/core/testing';

import { EditBlogService } from './edit-blog.service';

describe('EditBlogService', () => {
  let service: EditBlogService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EditBlogService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
