import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { blogpost } from '../models/blogpost';
@Injectable({
  providedIn: 'root'
})
export class BlogPostService {
  myAppUrl: string;
  myApiUrl: string;
  myDetailUrl: string;
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json; charset=utf-8'
    })
  }
  constructor(private http: HttpClient) { 

    this.myAppUrl = 'http://localhost:59005/';
      this.myApiUrl = 'api/home/getblog';
  }
  getBlogs(){
    return this.http.get(this.myAppUrl+this.myApiUrl)
  };
  getBlogPost(blogID: number): Observable<blogpost> {
    return this.http.get<blogpost>(this.myAppUrl + this.myApiUrl + blogID)
  };

}
