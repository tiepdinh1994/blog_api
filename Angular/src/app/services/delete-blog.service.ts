import { Injectable, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { blogpost } from '../models/blogpost';
import { Template } from '@angular/compiler/src/render3/r3_ast';


@Injectable({
  providedIn: 'root'
})
export class DeleteBlogService {
  myAppUrl: string;
  myApiUrl: string;

  constructor(private http: HttpClient) { 
    this.myAppUrl = 'http://localhost:59005/';
    this.myApiUrl = 'api/home/delete/';
  }
  deleteBlog(blogID: number): Observable<blogpost> {
    return this.http.delete<blogpost>(this.myAppUrl + this.myApiUrl + blogID);
  }
  createBlog(newBlog: blogpost, img): Observable<blogpost> {

    const fd = new FormData(); //li do phai dung fromform 
    fd.append('img', img)
    fd.append("blogTitle",newBlog.blogTitle)
    fd.append("blogContent",newBlog.blogContent)

    return this.http.post<blogpost>(this.myAppUrl + 'api/home/addBlogs/', fd);
  }
}

