import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { blogpost } from '../models/blogpost';

@Injectable({
  providedIn: 'root'
})
export class EditBlogService {
  myAppUrl: string;
  myApiUrl: string;

  constructor(private http: HttpClient) {
    this.myAppUrl = 'https://localhost:5000/';
    this.myApiUrl = 'api/home/edit/';
  }

}
