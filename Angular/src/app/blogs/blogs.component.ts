import { Component, OnInit } from '@angular/core';
import { BlogPostService } from '../services/blog-post.service';
import { Observable } from 'rxjs';
import { blogpost } from '../models/blogpost';

@Component({
  selector: 'app-blogs',
  templateUrl: './blogs.component.html',
  styleUrls: ['./blogs.component.css']
})
export class BlogsComponent implements OnInit {
  blogPosts: any;
  startIndex= 0;
  endIndex = 6;
  constructor(public blogPostService: BlogPostService) { }

  ngOnInit(): void {
    this.loadBlogs();
  }
  getArrayFromNumber(){
    var count=this.blogPosts.length;
    return count%6;
  };

  updateIndex(pageIndex){
    this.startIndex = pageIndex*6;
    this.endIndex = this.startIndex + 6;
  }

  loadBlogs() {
    this.blogPostService.getBlogs().subscribe(resp=>{
      console.log(resp)
      this.blogPosts = resp
    }, error=>{
      console.log(error)
    })
  }
}
