﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BackEnd_Blog.Entities
{
    public class User
    {
        [Key]
        public int userID { get; set; }
        public string userName { get; set; }
        public string passWord { get; set; }
        public ICollection<RoleUser> RoleUsers { get; set; }
    }
}
