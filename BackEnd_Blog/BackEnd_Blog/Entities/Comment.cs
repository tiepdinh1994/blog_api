﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace BackEnd_Blog.Models
{
    public class Comment
    {
        [Key]
        public int commentID { get; set; }
        [Required, MaxLength(1500)]
        public string content { get; set; }
        [Required]
        public DateTime? creatOn { get; set; }

        public int userID { get; set; }
        public int blogID { get; set; }
        public Blog Blogs { get; set; }
    }
}
