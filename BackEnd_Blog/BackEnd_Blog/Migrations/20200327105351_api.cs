﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace BackEnd_Blog.Migrations
{
    public partial class api : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Blogs",
                columns: table => new
                {
                    blogID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    blogTitle = table.Column<string>(maxLength: 100, nullable: false),
                    blogContent = table.Column<string>(maxLength: 3000, nullable: false),
                    authorID = table.Column<int>(nullable: false),
                    blogImages = table.Column<string>(nullable: true),
                    createOn = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Blogs", x => x.blogID);
                });

            migrationBuilder.CreateTable(
                name: "Comments",
                columns: table => new
                {
                    commentID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    content = table.Column<string>(maxLength: 1500, nullable: false),
                    creatOn = table.Column<DateTime>(nullable: false),
                    authorID = table.Column<int>(nullable: false),
                    blogID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Comments", x => x.commentID);
                    table.ForeignKey(
                        name: "FK_Comments_Blogs_blogID",
                        column: x => x.blogID,
                        principalTable: "Blogs",
                        principalColumn: "blogID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Comments_blogID",
                table: "Comments",
                column: "blogID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Comments");

            migrationBuilder.DropTable(
                name: "Blogs");
        }
    }
}
