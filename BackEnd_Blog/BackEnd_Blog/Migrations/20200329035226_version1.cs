﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BackEnd_Blog.Migrations
{
    public partial class version1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "blogComment",
                table: "Blogs");

            migrationBuilder.AddColumn<int>(
                name: "commentID",
                table: "Blogs",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "commentID",
                table: "Blogs");

            migrationBuilder.AddColumn<string>(
                name: "blogComment",
                table: "Blogs",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}
