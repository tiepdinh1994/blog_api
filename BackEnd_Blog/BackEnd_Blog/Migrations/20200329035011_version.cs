﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BackEnd_Blog.Migrations
{
    public partial class version : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "blogComment",
                table: "Blogs",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "blogComment",
                table: "Blogs");
        }
    }
}
