﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BackEnd_Blog.Migrations
{
    public partial class Unit8 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "authorID",
                table: "Comments");

            migrationBuilder.AddColumn<int>(
                name: "userID",
                table: "Comments",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "userID",
                table: "Comments");

            migrationBuilder.AddColumn<int>(
                name: "authorID",
                table: "Comments",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }
    }
}
