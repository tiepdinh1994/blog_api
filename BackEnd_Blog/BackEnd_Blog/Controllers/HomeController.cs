﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BackEnd_Blog.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace BackEnd_Blog.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class HomeController : ControllerBase
    {

        private readonly BlogDataContext _context;
        private readonly IBlogRepository _repo;
        public HomeController(BlogDataContext context, IBlogRepository repo)
        {
            _context = context;
            _repo = repo;
        }
        [HttpGet]
        [Route("getblog")]
        public async Task<IActionResult> getblog()
        {
            var blog = await _repo.getblog();
            return Ok(blog);

        }
        [HttpGet]
        [Route("detail/{id}")]
        public async Task<IActionResult> detail(int id)
        {
            if (ModelState.IsValid)
            {
                var b = await _context.Blogs
                              .Where(b => b.blogID == id)
                              .Include(b => b.Comments)
                              .FirstOrDefaultAsync();
                return Ok(b);
            }
            return BadRequest();
        }


        [HttpPost]
        [Route("add")]
        public async Task<IActionResult> addBlogs([FromForm] string blogTitle, [FromForm] string blogContent, [FromForm] IFormFile img)
        {
            Blog blog = new Blog
            {
                blogTitle = blogTitle,
                blogContent = blogContent
            };

            //load hinh o cho nay, truyen cai img vao la ok

            try
            {
                var newblog = new Blog()
                {
                    blogTitle = blog.blogTitle,
                    blogImages = blog.blogImages,
                    blogContent = blog.blogContent,
                    createOn = DateTime.Now
                };
                await _repo.add(newblog);
                await _context.SaveChangesAsync();
                return Ok(newblog);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [HttpDelete]
        [Route("delete/{id}")]
        public void delete(int id)
        {
            _repo.delete(id);
            _context.SaveChanges();
        }

        [HttpPut]
        [Route("edit/{id}")]
        public async Task<IActionResult> edit([FromBody]Blog blog)
        {
            _repo.edit(blog);
            await _context.SaveChangesAsync();
            return Ok(blog);
        }
    }

}
