﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BackEnd_Blog.Models
{
    public interface IBlogRepository
    {
        Task<List<Blog>> getblog();
        Task<int> add(Blog blog);
        public void delete(int id);
        public void edit(Blog blog);
        Task<List<Comment>> getComments(int id);
    }
}
