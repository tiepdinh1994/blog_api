﻿using BackEnd_Blog.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BackEnd_Blog.Services
{
    public interface ILoginRepository
    {
        List<UserViewModel> GetUser(int id);
    }
}
