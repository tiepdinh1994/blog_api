﻿using BackEnd_Blog.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BackEnd_Blog.Services
{
    public interface IUserRepository
    {
        User Find(string username, string password);
    }
}
