﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BackEnd_Blog.Models
{
    public interface ICommentRepository
    {
        Task<int> addComment(Comment coms);
        void deleteComments(Comment coms);
        Task<int> detailBlogs(int? id);
        Task updateComent(Comment coms);
        List<Comment> listComment();
    }
}
