﻿using BackEnd_Blog.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BackEnd_Blog
{
    public class BlogRepository : IBlogRepository
    {
        private readonly BlogDataContext _context;


        public BlogRepository(BlogDataContext context)
        {
            _context = context;
        }


        //get listblog
        public async Task<List<Blog>> getblog()
        {
            if (_context != null)
            {
                return await _context.Blogs.ToListAsync();
            }
            return null;
        }


        //add done
        public async Task<int> add(Blog blog)
        {
            var b = new Blog()
            {
                blogTitle = blog.blogTitle,
                blogContent = blog.blogContent,
                blogImages = blog.blogImages,
                createOn = blog.createOn,
            };
            _context.Blogs.Add(b);
            return await _context.SaveChangesAsync();
        }



        //delete
        public void delete(int id) //
        {

            var comments = _context.Comments.Where(c => c.blogID == id).ToList();
            _context.Comments.RemoveRange(comments);
            var blog = _context.Blogs.Find(id);
            if (blog != null)
            {
                _context.Blogs.Remove(blog);
            }
            _context.SaveChanges();


        }
        public void edit(Blog blog)
        {
            IEnumerable<Blog> check = _context.Blogs.Where(p => p.blogID == blog.blogID).ToList();
            foreach (var x in check)
            {
                x.blogID = blog.blogID;
                x.authorID = blog.authorID;
                x.blogTitle = blog.blogTitle;
                x.blogContent = blog.blogContent;
                x.blogImages = blog.blogImages;
            }
            _context.SaveChanges();
        }
        public Task<List<Comment>> getComments(int id)
        {
            throw new NotImplementedException();
        }
    }
}