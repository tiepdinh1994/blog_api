﻿using BackEnd_Blog.Models;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Threading.Tasks;

namespace BackEnd_Blog.Services
{
    public class LoginRepository : ILoginRepository
    {
        public static IEnumerable<dynamic> GetData(String cmdText)
        {
            using (var connection = new SqlConnection("Server=VIETTIEP-PC\\SQLEXPRESS;Database=API_NashTech;integrated security=yes;"))
            {
                connection.Open();

                using (var command = new SqlCommand(cmdText, connection))
                {
                    using (var dataReader = command.ExecuteReader())
                    {
                        var fields = new List<String>();

                        for (var i = 0; i < dataReader.FieldCount; i++)
                        {
                            fields.Add(dataReader.GetName(i));
                        }

                        while (dataReader.Read())
                        {
                            var item = new ExpandoObject() as IDictionary<String, Object>;

                            for (var i = 0; i < fields.Count; i++)
                            {
                                item.Add(fields[i], dataReader[fields[i]]);
                            }

                            yield return item;
                        }
                    }
                }
            }
        }

        public List<UserViewModel> GetUser(int id)
        {
            var data = GetData("EXEC GetUser " + id).ToList();
            var list = new List<UserViewModel>();
            foreach (dynamic temp in data)
            {
                list.Add(new UserViewModel
                {
                    Name = temp.Name,
                    role = temp.rol,
                    UserID = temp.UserID

                });
            }
            return list;
        }
    }
}
