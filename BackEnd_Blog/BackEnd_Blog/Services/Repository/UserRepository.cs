﻿using BackEnd_Blog.Entities;
using BackEnd_Blog.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BackEnd_Blog.Services
{
    public class UserRepository : IUserRepository
    {
        private BlogDataContext _context;
        public UserRepository(BlogDataContext context)
        {
            _context = context;
        }
        public User Find(string username, string password)
        {
            User user = new User();
            user = _context.Users.Where(m => m.userName == username && m.passWord == password).FirstOrDefault();
            return user;
        }
    }
}
