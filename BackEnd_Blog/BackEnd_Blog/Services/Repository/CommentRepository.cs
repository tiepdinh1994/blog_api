﻿using BackEnd_Blog.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BackEnd_Blog
{
    public class CommentRepository : ICommentRepository
    {
        private readonly BlogDataContext _context;
        public CommentRepository(BlogDataContext context)
        {
            _context = context;
        }

        public async Task<int> addComment(Comment coms)
        {
            if (_context != null)
            {
                await _context.AddAsync(coms);
                await _context.SaveChangesAsync();
                return coms.commentID;
            }
            return 0;
        }

        public void deleteComments(int? id)
        {
            IEnumerable<Comment> listCom = _context.Comments.Where(p => p.commentID == id).ToList();
            foreach( var c in listCom)
            {
                _context.Remove(c);
                _context.SaveChanges();
            }

        }

        public void deleteComments(Comment coms)
        {
            throw new NotImplementedException();
        }

        public Task<int> detailBlogs(int? id)
        {
            throw new NotImplementedException();
        }

        public List<Comment> listComment()
        {
            throw new NotImplementedException();
        }

        public Task updateComent(Comment coms)
        {
            throw new NotImplementedException();
        }
    }
  

}
