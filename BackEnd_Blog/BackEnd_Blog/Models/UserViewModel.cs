﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BackEnd_Blog.Models
{
    public class UserViewModel
    {
        public string Name { get; set; }
        public string role { get; set; }
        public int UserID { get; set; }
    }
}
