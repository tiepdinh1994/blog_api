﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BackEnd_Blog.Models
{
    public class PageViewModel<T>
    {
        public List<T> Items { get; set; }
        public int totalRecored { get; set; }
    }
}
