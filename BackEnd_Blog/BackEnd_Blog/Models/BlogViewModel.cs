﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BackEnd_Blog.Models
{
    public class BlogViewModel
    {
        public string blogTitle { get; set; }
        public string blogContent { get; set; }
        public int authorID { get; set; }
        public string blogImages { get; set; }
        public string blogComment { get; set; }
        public DateTime? createOn { get; set; }
        public ICollection<Comment> Comments { get; set; }
    }
}
